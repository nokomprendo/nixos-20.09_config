{ pkgs, ... }: {

  home.packages = with pkgs; [

    # overlays
    myhello
    nano

    # with packages
    (python3.withPackages (ps: with ps; [
      numpy
    ]))

    # packages
    cabal-install
    geany

  ];

}

