{ pkgs, ... }: {

  programs.vscode = {
    enable = true;

    extensions = with pkgs.vscode-extensions; [

      bbenoist.Nix

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "vscode-ghc-simple";
          publisher = "dramforever";
          version = "0.1.22";
          sha256 = "0x3csdn3pz5rhl9mhplpm8kxb40l1dw5rnwhh3zsif3rz0nqhk2a";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "language-haskell";
          publisher = "JustusAdam";
          version = "3.3.0";
          sha256 = "1285bs89d7hqn8h8jyxww7712070zw2ccrgy6aswd39arscniffs";
        };
      })

    ];

      # userSettings = {
      #   # general settings
      #   "window.zoomLevel" = 1.25;
      #   "editor.fontSize" = 13;
      #   "keyboard.dispatch" = "keyCode";
      #   "update.enableWindowsBackgroundUpdates" = false;
      #   "update.mode" = "none";
      # };

  };

}

