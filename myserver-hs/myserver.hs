{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}

import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty

main :: IO ()
main = do
    putStrLn $ "GHC" ++ show (__GLASGOW_HASKELL__ :: Int)
    scotty 5000 $ do
        middleware logStdoutDev
        get "/" $ text "Hello Nix !"

