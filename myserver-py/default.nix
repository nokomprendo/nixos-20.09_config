{ pkgs ? import <nixpkgs> {} }:

with pkgs.python3Packages; buildPythonApplication {
  pname = "myserver";
  src = ./.;
  version = "0.1";
  propagatedBuildInputs = [ flask ];
}

