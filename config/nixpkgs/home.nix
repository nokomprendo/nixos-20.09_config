{ pkgs, ... }: {

  imports = [
    ./git.nix
    ./packages.nix
    ./vim.nix
    ./vscode.nix
  ];

  home = {
    username = "toto";
    homeDirectory = "/home/toto";
    stateVersion = "20.09";
  };

  programs = {

    direnv = {
      enable = true;
      enableNixDirenvIntegration = true;
    };

    home-manager.enable = true;
    bash.enable = true;

  };

}

