{ pkgs, ... }: {

  programs.git = {
    enable = true;
    userName = "toto";
    userEmail = "toto@example.com";
    ignores =  [
      "*~"
      "*.swp"
    ];

    extraConfig = {
      pull.rebase = false;
      credential.helper = "cache --timeout=10800";
    };

  };

}


