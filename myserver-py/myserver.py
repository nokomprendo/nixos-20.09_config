#!/usr/bin/env python3
from flask import Flask
import sys

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello, Nix!"

if __name__ == "__main__":
    print("using {}".format(sys.version))
    app.run(host="0.0.0.0")

