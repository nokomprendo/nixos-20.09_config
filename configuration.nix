{ config, pkgs, ... }: {

  imports = [
    ./hardware-configuration.nix
  ];

  boot.loader.grub = {
    enable = true;
    device = "/dev/sda";
    version = 2;
  };  

  time.timeZone = "Europe/Paris";
  system.stateVersion = "20.09";
  i18n.defaultLocale = "fr_FR.UTF-8";

  networking = {
    hostName = "nixos";
    useDHCP = false;
    interfaces.enp0s3.useDHCP = true;
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr-bepo";
  };

  environment.systemPackages = with pkgs; [
    firefox
    git
    vim
  ];

  services = {

    xserver = {
      enable = true;
      layout = "fr";
      xkbVariant = "bepo";
      displayManager.lightdm.enable = true;
      desktopManager.xfce.enable = true;
    };

  };

  # nix-direnv
  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';

  # docker
  virtualisation.docker.enable = true;

  # user
  users.users.toto = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" ];
  };

}

