let
  rev = "20.03";
  url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  pkgs = import (fetchTarball url) {};
in import ./default.nix { inherit pkgs; }

